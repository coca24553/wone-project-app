import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:wone_app/login_check.dart';
import 'package:wone_app/pages/member/add_new_card_screen.dart';
import 'package:wone_app/pages/answer/answer_create_page.dart';
import 'package:wone_app/pages/answer/answer_write_page.dart';
import 'package:wone_app/pages/service/calendar_page.dart';
import 'package:wone_app/pages/member/join_membership_page.dart';
import 'package:wone_app/pages/member/log_in_page.dart';
import 'package:wone_app/pages/service/main_page.dart';
// import 'package:wone_app/pages/member_detail_page.dart';
import 'package:wone_app/pages/member/member_info_page.dart';
import 'package:wone_app/pages/member/member_page.dart';
import 'package:wone_app/pages/board/notice_page.dart';
import 'package:wone_app/pages/page_index.dart';
import 'package:wone_app/pages/member/setting_page.dart';
import 'package:wone_app/pages/service/spending_page.dart';


void main() async {
  await initializeDateFormatting();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.light(
          surfaceTint: Colors.white, // 배경색
          primary: Color(0xff3498db), // 헤더 백그라운드 색
          // onPrimary: Colors.black, // 헤더 텍스트 색
          onSurface: Colors.black // body 텍스트 색
        ),
      ),
      home: LoginCheck(),
    );
  }
}