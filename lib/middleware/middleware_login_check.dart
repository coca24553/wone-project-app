import 'package:flutter/material.dart';
import 'package:wone_app/functions/token_lib.dart';
import 'package:wone_app/pages/member/log_in_page.dart';
import 'package:wone_app/pages/member/member_page.dart';
import 'package:wone_app/pages/page_index.dart';

class MiddlewareLoginCheck {
  void check(BuildContext context) async {
    String? memberToken = await TokenLib.getMemberToken();
    if (memberToken == null) {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const LogInPage()),
          (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
              builder: (BuildContext context) => const PageIndex()),
          (route) => false);
    }
  }
}
