import 'package:adaptive_dialog/adaptive_dialog.dart';
import 'package:flutter/material.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/config/config_style.dart';
import 'package:wone_app/functions/token_lib.dart';
import 'package:wone_app/model/member/member_info_response.dart';
import 'package:wone_app/pages/member/add_new_card_screen.dart';
import 'package:wone_app/pages/member/change_card_screen.dart';
import 'package:wone_app/pages/member/log_in_page.dart';
import 'package:wone_app/repository/repo_member_info.dart';
import '../page_index.dart';


class MemberPage extends StatefulWidget {
  const MemberPage({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<MemberPage> createState() => _MainPageState();
}

class _MainPageState extends State<MemberPage> {
  DateTime date = DateTime.now(); // 선택한 날짜를 입력받을 변수 선언
  String input = "";
  MemberInfoResponse? _memberInfo;

  Future<void> _loadMemberInfo() async {
    await RepoMemberInfo().getMember(widget.id)
        .then((res) => {
      setState(() {
        _memberInfo = res.data;
      })
    });
  }

  void initState(){
    super.initState();
    _loadMemberInfo();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        foregroundColor: colorPrimary,// 앱바 기본 아이콘 색
        shadowColor: Colors.black.withOpacity(0.5),
        elevation: 2,
        title: Text(
          "개인정보",
          style: TextStyle(
            fontFamily: 'NotoSans_Bold',
            fontSize: appbarFontSize,
            letterSpacing: -0.5,
            color: colorPrimary,
          ),
        ),
        actions: [
          Icon(
            Icons.notifications,
            color: colorPrimary,
            size: appbarIconSize,
          )
        ],
      ),
      body: _buildBody(context),
      resizeToAvoidBottomInset: false, // 키보드 올라왔을 때 화면 깨짐 현상 없애줌.
    );
  }

  Future<void> _logout() async {
    TokenLib.logout(context);
  }

  Widget _buildBody(BuildContext context) {
    double mediaQueryWidth = MediaQuery.of(context).size.width;
    double mediaQueryHeight = MediaQuery.of(context).size.height;

    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            // 아이콘
            Container(
              margin: EdgeInsets.fromLTRB(0, 50, 0, 30),
              height: mediaQueryWidth * 0.25,
              width: mediaQueryWidth * 0.25,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: colorPrimary,
              ),
              child: Icon(
                Icons.person,
                color: Colors.white,
                size: mediaQueryWidth * 0.18,
              ),
            ),
            // 개인정보 내용
            Container(
              margin: EdgeInsets.fromLTRB(20, 8, 20, 0),
              width: mediaQueryWidth,
              decoration: BoxDecoration(
                color: colorLightGrey,
                borderRadius: BorderRadius.all(
                  Radius.circular(boxRadius),
                ),
              ),
              child: Column(
                children: [
                  // 이름 입력 폼
                  Container(
                    height: mediaQueryHeight * 0.075,
                    child: Row(
                      children: [
                        // 이름
                        Container(
                          width: mediaQueryWidth * 0.4,
                          margin: edgeInsetsMember,
                          child: Text(
                            '이름',
                            style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeMid,
                                color: colorLightBlack,
                            ),
                          ),
                        ),
                        // 홍길동
                        Container(
                            width: mediaQueryWidth * 0.41,
                          height: mediaQueryHeight,
                          alignment: FractionalOffset.centerRight,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              TextButton(
                                onPressed: () {
                                  showDialog(
                                      context: context,
                                      barrierDismissible: false, // 바깥 영역 터치시 닫을지 여부
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          content: TextField(
                                            decoration: InputDecoration(
                                              hintText: '변경할 이름을 입력해주세요.',
                                              //hintText: _memberInfo!.username,
                                              hintStyle: TextStyle(
                                                fontSize: fontSizeMid,
                                                letterSpacing: fontLetterSpacing,
                                                color: colorGrey,
                                              ),
                                            ),
                                          ),
                                          actions: [
                                            TextButton(
                                              child: const Text('취소'),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                            TextButton(
                                              child: const Text('확인'),
                                              onPressed: () {
                                                Navigator.of(context).pop();
                                              },
                                            ),
                                          ],
                                        );
                                      }
                                  );
                                },
                                child: Row(
                                  children: [
                                    Text(
                                      _memberInfo!.username,
                                      style: TextStyle(
                                        letterSpacing: fontLetterSpacing,
                                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                                        fontSize: fontSizeMid,
                                        color: colorDarkGrey,
                                      ),
                                    ),
                                    // 아이콘
                                    Icon(
                                      Icons.chevron_right,
                                      color: colorDarkGrey,
                                    ),
                                  ],
                                )
                              ),
                            ],
                          )
                        ),
                      ],
                    ),
                  ),

                  // 경계선
                  Container(
                    height: 1,
                    color: Colors.grey.withOpacity(0.2),
                  ),

                  // 생년월일 입력 폼
                  Container(
                    height: mediaQueryHeight * 0.075,
                    child: Row(
                      children: [
                        Container(
                          width: mediaQueryWidth * 0.4,
                          margin: edgeInsetsMember,
                          child: Text(
                            '생년월일',
                            style: TextStyle(
                              letterSpacing: fontLetterSpacing,
                              fontFamily: 'NotoSans_Bold',
                              fontSize: fontSizeMid,
                              color: colorLightBlack,
                            ),
                          ),
                        ),
                        Container(
                            width: mediaQueryWidth * 0.41,
                            height: mediaQueryHeight,
                            alignment: FractionalOffset.centerRight,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                TextButton(
                                    onPressed: () {},
                                    child: Row(
                                      children: [
                                        Text(
                                          '2000-01-15',
                                          style: TextStyle(
                                            letterSpacing: fontLetterSpacing,
                                            fontFamily: 'NotoSans_NotoSansKR-Regular',
                                            fontSize: fontSizeMid,
                                            color: colorDarkGrey,
                                          ),
                                        ),
                                        Icon(
                                          Icons.chevron_right,
                                          color: colorDarkGrey,
                                        ),
                                      ],
                                    )
                                ),
                              ],
                            )
                        ),
                      ],
                    ),
                  ),

                  // 경계선
                  Container(
                    height: 1,
                    color: Colors.grey.withOpacity(0.2),
                  ),

                  // 비밀번호 입력 폼
                  Container(
                    height: mediaQueryHeight * 0.075,
                    child: Row(
                      children: [
                        // 비밀번호
                        Container(
                          width: mediaQueryWidth * 0.4,
                          margin: edgeInsetsMember,
                          child: Text(
                            '비밀번호',
                            style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeMid,
                              color: colorLightBlack,
                            ),
                          ),
                        ),
                        // ****
                        Container(
                          width: mediaQueryWidth * 0.41,
                          alignment: FractionalOffset.centerRight,
                          child: TextButton(
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  barrierDismissible: false, // 바깥 영역 터치시 닫을지 여부
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      content: TextField(
                                        decoration: InputDecoration(
                                            hintText: '변경할 비밀번호를 입력해주세요.',
                                        hintStyle: TextStyle(
                                          letterSpacing: fontLetterSpacing,
                                          fontFamily: 'NotoSans_NotoSansKR-Regular',
                                          fontSize: fontSizeMid,
                                          color: colorDarkGrey,
                                        )
                                        ),
                                        obscureText: true, //비밀번호 안 보이게 함
                                      ),
                                      actions: [
                                        TextButton(
                                          child: const Text('취소'),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        TextButton(
                                          child: const Text('확인'),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                      ],
                                    );
                                  }
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  '****',
                                  style: TextStyle(
                                    letterSpacing: fontLetterSpacing,
                                    fontFamily: 'NotoSans_NotoSansKR-Regular',
                                    fontSize: fontSizeBig,
                                    color: colorLightBlack,
                                  ),
                                ),
                                // 아이콘
                                Icon(
                                  Icons.chevron_right,
                                  color: colorDarkGrey,
                                ),
                              ],
                            )
                          ),
                        ),
                      ],
                    ),
                  ),

                  // 경계선
                  Container(
                    height: 1,
                    color: Colors.grey.withOpacity(0.2),
                  ),

                  // 카드 입력 폼
                  Container(
                    height: mediaQueryHeight * 0.075,
                    child: Row(
                      children: [
                        // 카드
                        Container(
                          width: mediaQueryWidth * 0.4,
                          margin: edgeInsetsMember,
                          child: Text(
                            '카드',
                            style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeMid,
                              color: colorLightBlack,
                            ),
                          ),
                        ),
                        // 현재 카드
                        Container(
                          width: mediaQueryWidth * 0.41,
                          alignment: FractionalOffset.centerRight,
                          child: TextButton(
                            onPressed: () {
                              showDialog(
                                  context: context,
                                  barrierDismissible: false, // 바깥 영역 터치시 닫을지 여부
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      content: Text(
                                        '현재 사용 중인 카드를\n변경하시겠습니까?',
                                        style: TextStyle(
                                          fontSize: fontSizeMid,
                                          letterSpacing: fontLetterSpacing,
                                          color: colorLightBlack,
                                        ),
                                      ),
                                      actions: [
                                        TextButton(
                                          child: const Text('취소'),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                        ),
                                        TextButton(
                                          child: const Text('확인'),
                                          onPressed: () {
                                            Navigator.of(context).push(MaterialPageRoute(builder: (context) => ChangeCardScreen()));
                                          },
                                        ),
                                      ],
                                    );
                                  }
                              );
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Text(
                                  '농협',
                                  style: TextStyle(
                                    letterSpacing: fontLetterSpacing,
                                    fontFamily: 'NotoSans_NotoSansKR-Regular',
                                    fontSize: fontSizeMid,
                                    color: colorDarkGrey,
                                  ),
                                ),
                                // 아이콘
                                Icon(
                                  Icons.chevron_right,
                                  color: colorDarkGrey,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            // 로그아웃 하기
            Container(
              alignment: FractionalOffset.center,
              margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
              width: mediaQueryWidth,
              height: mediaQueryHeight * 0.075,
              decoration: BoxDecoration(
                color: colorLightGrey,
                borderRadius: BorderRadius.all(
                  Radius.circular(boxRadius),
                ),
              ),
              child: Container(
                width: mediaQueryWidth,
                child: TextButton(
                  onPressed: () {
                    showDialog(
                        context: context,
                        barrierDismissible: false, // 바깥 영역 터치시 닫을지 여부
                        builder: (BuildContext context) {
                          return AlertDialog(
                            content: Text(
                              '정말 로그아웃 하시겠습니까?',
                              style: TextStyle(
                                  fontFamily: 'NotoSans_NotoSansKR-Regular',
                                  fontSize: fontSizeMid,
                                  letterSpacing: fontLetterSpacing,
                                  color: colorLightBlack,
                              ),
                            ),
                            actions: [
                              TextButton(
                                child: const Text('취소'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              TextButton(
                                child: const Text('확인'),
                                onPressed: () async {
                                  _logout();
                                  //Navigator.of(context).push(MaterialPageRoute(builder: (context) => LogInPage()));
                                },
                              ),
                            ],
                          );
                        }
                    );
                  },
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                  ),
                  child: Text(
                    '로그아웃 하기',
                    style: TextStyle(
                        letterSpacing: fontLetterSpacing,
                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                        fontSize: fontSizeSm,
                      color: colorDarkGrey,
                    ),
                  ),
                ),
              ),
            ),
            // 회원탈퇴 하기
            Container(
              alignment: FractionalOffset.center,
              margin: EdgeInsets.fromLTRB(20, 10, 20, 0),
              width: mediaQueryWidth,
              height: mediaQueryHeight * 0.075,
              decoration: BoxDecoration(
                color: colorLightGrey,
                borderRadius: BorderRadius.all(
                  Radius.circular(boxRadius),
                ),
              ),
              child: Container(
                width: mediaQueryWidth,
                child: TextButton(
                  onPressed: () {
                    showDialog(
                        context: context,
                        barrierDismissible: false, // 바깥 영역 터치시 닫을지 여부
                        builder: (BuildContext context) {
                          return AlertDialog(
                            content: Text(
                              '정말 회원탈퇴 하시겠습니까?',
                              style: TextStyle(
                                fontFamily: 'NotoSans_NotoSansKR-Regular',
                                fontSize: fontSizeMid,
                                letterSpacing: fontLetterSpacing,
                                color: colorLightBlack,
                              ),
                            ),
                            actions: [
                              TextButton(
                                child: const Text('취소'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                              TextButton(
                                child: const Text('확인'),
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                              ),
                            ],
                          );
                        }
                    );
                  },
                  style: TextButton.styleFrom(
                    padding: EdgeInsets.zero,
                  ),
                  child: Text(
                    '회원탈퇴 하기',
                    style: TextStyle(
                        letterSpacing: fontLetterSpacing,
                        fontFamily: 'NotoSans_NotoSansKR-Regular',
                        fontSize: fontSizeSm,
                      color: colorDarkGrey,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
}