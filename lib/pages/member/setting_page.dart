import 'package:flutter/material.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/config/config_style.dart';
import 'package:wone_app/model/member/member_info_response.dart';
import 'package:wone_app/pages/answer/answer_create_page.dart';
import 'package:wone_app/pages/board/guide_page.dart';
import 'package:wone_app/pages/member/member_info_page.dart';
import 'package:wone_app/pages/member/member_page.dart';
import 'package:wone_app/pages/board/notice_page.dart';
import 'package:wone_app/repository/repo_member_info.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({
    super.key,
    required this.id,
  });

  final num id;

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  MemberInfoResponse? _memberInfo;

  Future<void> _loadMemberInfo() async {
    await RepoMemberInfo().getMember(widget.id)
        .then((res) => {
      setState(() {
        _memberInfo = res.data;
      })
    });
  }

  void initState(){
    super.initState();
    _loadMemberInfo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        foregroundColor: colorPrimary,// 앱바 기본 아이콘 색
        shadowColor: Colors.black.withOpacity(0.5),
        elevation: 2,
        title: Text(
          "설정",
          style: TextStyle(
            fontFamily: 'NotoSans_Bold',
            fontSize: appbarFontSize,
            letterSpacing: -0.5,
            color: colorPrimary,
          ),
        ),
        actions: [
          Icon(
            Icons.notifications,
            color: colorPrimary,
            size: appbarIconSize,
          )
        ],
      ),
      body: _buildBody(context),
      resizeToAvoidBottomInset: false, // 키보드 올라왔을 때 화면 깨짐 현상 없애줌.
    );
  }
  Widget _buildBody(BuildContext context) {
    double mediaQueryWidth = MediaQuery.of(context).size.width;
    double mediaQueryHeight = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      child: Column(
        children: [
          /** 상단 개인정보 **/
          Container(
            margin: EdgeInsets.all(20),
            width: mediaQueryWidth,
            height: mediaQueryHeight * 0.18,
            child: TextButton(
                style: OutlinedButton.styleFrom(
                  backgroundColor: colorLightGrey,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.all(
                      Radius.circular(boxRadius),
                    ),
                  ),
                ),
                // memberId 입력해줘야만 넘어감
                onPressed: () async {
                  //Navigator.of(context).push(MaterialPageRoute(builder: (context) => MemberInfoPage(id: 2)));
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => MemberPage(id: 20)));
                },
              child: Row(
                children: [
                  /** n일째 도전 중 아이콘 **/
                  Container(
                    height: mediaQueryWidth * 0.11,
                    width: mediaQueryWidth * 0.11,
                    decoration: const BoxDecoration(
                      shape: BoxShape.circle,
                      color: colorPrimary,
                    ),
                    child: Icon(
                      Icons.hourglass_bottom,
                      color: Colors.white,
                      size: mediaQueryWidth * 0.07,
                    ),
                  ),
                  // 회원 정보
                  Container(
                    margin: EdgeInsets.only(left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        /** 멤버 이름 **/
                        Container(
                          width: mediaQueryWidth / 2,
                          child: Row(
                            children: [
                              Container(
                                child: Row(
                                  children: [
                                    Text(
                                      _memberInfo!.username,
                                      //'김철수',
                                      style: TextStyle(
                                        fontFamily: 'NotoSans_Bold',
                                        fontSize: fontSizeBig,
                                        letterSpacing: fontLetterSpacing,
                                        color: colorPrimary,
                                      ),
                                    ),
                                    Icon(
                                      Icons.chevron_right,
                                      color: colorPrimary,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        /** 15일 째 도전중 **/
                        Container(
                          width: mediaQueryWidth / 2,
                          child: Text(
                            '+1일째 도전 중',
                            style: TextStyle(
                              fontFamily: 'assets/fonts/NotoSansKR-Regular',
                              fontSize: fontSizeSm,
                              letterSpacing: fontLetterSpacing,
                              color: colorGrey,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          /** 문의 **/
          Container(
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(25, 0, 0, 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                        child: Icon(
                          Icons.help_outline,
                          color: colorPrimary,
                          size: mediaQueryWidth * 0.09,
                        ),
                      ),
                      Container(
                        child: Text(
                          "문의",
                          style: TextStyle(
                            fontFamily: 'NotoSans_Bold',
                            fontSize: fontSize22,
                            letterSpacing: fontLetterSpacing,
                            color: colorPrimary,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                /** 이용방법 **/
                Container(
                  margin: EdgeInsets.fromLTRB(10, 0, 13, 10),
                  child: TextButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => const GuidePage()));
                      },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          child: Row(
                            children: [
                              Container(
                                margin: edgeInsetsSettingList,
                                child: Icon(
                                  Icons.local_library_outlined,
                                  color: colorDarkGrey,
                                  size: mediaQueryWidth * settingIconSize,
                                ),
                              ),
                              Container(
                                child: Text(
                                  "이용방법",
                                  style: TextStyle(
                                    fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                    fontSize: fontSizeBig,
                                    letterSpacing: fontLetterSpacing,
                                    color: colorDarkGrey,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          child: Icon(
                            Icons.chevron_right,
                            color: colorDarkGrey,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                /** 공지사항 **/
                Container(
                  margin: EdgeInsets.fromLTRB(10, 0, 13, 10),
                  child: TextButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => NoticePage()));
                      },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: mediaQueryWidth * 0.5,
                          child: Row(
                            children: [
                              Container(
                                margin: edgeInsetsSettingList,
                                child: Icon(
                                  Icons.check_circle_outline,
                                  color: colorDarkGrey,
                                  size: mediaQueryWidth * settingIconSize,
                                ),
                              ),
                              Container(
                                child: Text(
                                  "공지사항",
                                  style: TextStyle(
                                    fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                    fontSize: fontSizeBig,
                                    letterSpacing: fontLetterSpacing,
                                    color: colorDarkGrey,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: FractionalOffset.centerRight,
                          width: mediaQueryWidth * 0.35,
                          child: Icon(
                            Icons.chevron_right,
                            color: colorDarkGrey,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                /** 고객센터 **/
                Container(
                  margin: EdgeInsets.fromLTRB(10, 0, 13, 10),
                  child: TextButton(
                      onPressed: () {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => AnswerCreatePage()));
                      },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          width: mediaQueryWidth * 0.5,
                          child: Row(
                            children: [
                              Container(
                                margin: edgeInsetsSettingList,
                                child: Icon(
                                  Icons.headset_outlined,
                                  color: colorDarkGrey,
                                  size: mediaQueryWidth * settingIconSize,
                                ),
                              ),
                              Container(
                                child: Text(
                                  "고객센터",
                                  style: TextStyle(
                                    fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                    fontSize: fontSizeBig,
                                    letterSpacing: fontLetterSpacing,
                                    color: colorDarkGrey,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          alignment: FractionalOffset.centerRight,
                          width: mediaQueryWidth * 0.35,
                          child: Icon(
                            Icons.chevron_right,
                            color: colorDarkGrey,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
