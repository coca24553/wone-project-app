import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_form_builder/flutter_form_builder.dart';
import 'package:form_builder_validators/form_builder_validators.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_form_validator.dart';
import 'package:wone_app/config/config_path.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/config/config_style.dart';
import 'package:wone_app/functions/token_lib.dart';
import 'package:wone_app/middleware/middleware_login_check.dart';
import 'package:wone_app/model/member/login_request.dart';
import 'package:wone_app/pages/member/join_membership_page.dart';
import 'package:wone_app/pages/page_index.dart';
import 'package:wone_app/repository/repo_member.dart';

class LogInPage extends StatefulWidget {
  const LogInPage({Key? key}) : super(key: key);

  @override
  State<LogInPage> createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
  final _formKey = GlobalKey<FormBuilderState>();

  Future<void> _doLogin(LoginRequest loginRequest) async {
    await RepoMember().doLogin(loginRequest).then((res) {
      // api에서 받아온 결과값을 token에 넣는다.
      TokenLib.setMemberToken(res.data.token);
      TokenLib.setMemberName(res.data.name);
      // 미들웨어에게 부탁해서 토큰값 여부 검사 후 페이지 이동을 부탁한다.
      MiddlewareLoginCheck().check(context);
    }).catchError((err) {
      debugPrint(err);
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    double mediaQueryWidth = MediaQuery.of(context).size.width;
    double mediaQueryHeight = MediaQuery.of(context).size.width;

    //final _formKey = GlobalKey<FormState>();
    //final _formKey = GlobalKey<FormBuilderState>();

    return SingleChildScrollView(
      child: // 로그인 화면
      Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height * 0.9,
        // alignment: FractionalOffset.center,
        child: Column(
          // mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // 메인 로고
            Container(
              margin: EdgeInsets.fromLTRB(0, 0, 25, 15),
              child: Image.asset(
                '${mainLogo}',
                width: mediaQueryWidth * 0.35,
                height: mediaQueryHeight * 0.35,
              ),
            ),

            // 입력 폼
            Container(
              child: FormBuilder(
                key: _formKey,
                autovalidateMode: AutovalidateMode.disabled,
                child: Column(
                  children: [
                    /** 아이디 입력 **/
                    Container(
                      margin: edgeInsetsLogIn,
                      width: mediaQueryWidth * widthLogIn,
                      child: FormBuilderTextField(

                        name: 'userId',
                        maxLength: 20,
                        keyboardType: TextInputType.text,
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(errorText: formErrorRequired),
                          FormBuilderValidators.minLength(5, errorText: formErrorMinLength(5)),
                          FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                        ]),

                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: Colors.grey.withOpacity(0.5)
                            ),
                            borderRadius: BorderRadius.circular(buttonRadius),
                          ),
                          contentPadding: EdgeInsets.only(left: 15), // 텍스프 필드 내부 패딩
                          counterText: '', // counter text를 비움으로 설정
                          hintText: '아이디',
                          hintStyle: TextStyle(
                            letterSpacing: fontLetterSpacing,
                            color: colorGrey,
                          ),
                          errorStyle: TextStyle(
                            fontSize: 12,
                            letterSpacing: -0.5
                          ),
                        ),
                      ),
                    ),
                    /** 비밀번호 입력 **/
                    Container(
                      margin: edgeInsetsLogIn,
                      width: mediaQueryWidth * widthLogIn,
                      child: FormBuilderTextField(
                        obscureText: true,
                        name: 'password',
                        maxLength: 20,
                        validator: FormBuilderValidators.compose([
                          FormBuilderValidators.required(errorText: formErrorRequired),
                          FormBuilderValidators.minLength(6, errorText: formErrorMinLength(6)),
                          FormBuilderValidators.maxLength(20, errorText: formErrorMaxLength(20)),
                        ]),

                        decoration: InputDecoration(
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(buttonRadius),
                          ),
                          contentPadding: EdgeInsets.only(left: 15), // 텍스프 필드 내부 패딩
                          counterText: '', // counter text를 비움으로 설정
                          hintText: '비밀번호',
                          hintStyle: TextStyle(
                            letterSpacing: fontLetterSpacing,
                            color: colorGrey,
                          ),
                          errorStyle: TextStyle(
                              fontSize: 12,
                              letterSpacing: -0.5
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),

            // 로그인 버튼
            Container(
              margin: edgeInsetsLogIn,
              width: mediaQueryWidth * widthLogIn,
              height: 45,
              child: ElevatedButton(
                  style: OutlinedButton.styleFrom(
                    backgroundColor: colorPrimary,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(buttonRadius),
                    ),
                  ),
                  onPressed: () {
                    if(_formKey.currentState!.saveAndValidate()) {
                      LoginRequest loginRequest = LoginRequest(
                        _formKey.currentState!.fields['userId']!.value,
                        _formKey.currentState!.fields['password']!.value,
                      );
                      _doLogin(loginRequest);
                    }
                    //Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageIndex()));
                  },
                  child: const Text(
                    '로그인',
                    style: TextStyle(
                        letterSpacing: fontLetterSpacing,
                        fontFamily: 'NotoSans_Bold',
                      color: colorInsideFont
                    ),
                  )
              ),
            ),

            // 회원가입 이동 버튼
            Container(
              margin: edgeInsetsLogIn,
              width: mediaQueryWidth * widthLogIn,
              height: 45,
              child: ElevatedButton(
                  style: OutlinedButton.styleFrom(
                      backgroundColor: colorLightGrey,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(buttonRadius),
                      )
                  ),
                  onPressed: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (context) => JoinMembershipPage()));
                  },
                  child: Text(
                    '회원가입',
                    style: TextStyle(
                        letterSpacing: fontLetterSpacing,
                        fontFamily: 'NotoSans_Bold',
                      color: colorPrimary
                    ),
                  )
              ),
            ),
          ],
        ),
      )
    );
  }
}
