import 'package:flutter/material.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_path.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/config/config_style.dart';
import 'package:wone_app/pages/member/add_new_card_screen.dart';
import 'package:wone_app/pages/page_index.dart';

class JoinMembershipPage extends StatefulWidget {
  const JoinMembershipPage({super.key});

  @override
  State<JoinMembershipPage> createState() => _JoinMembershipPageState();
}

class _JoinMembershipPageState extends State<JoinMembershipPage> {
  DateTime date = DateTime.now(); // 선택한 날짜를 입력받을 변수 선언

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context),
    );
  }
  Widget _buildBody(BuildContext context) {
    double mediaQueryWidth = MediaQuery.of(context).size.width;
    double mediaQueryHeight = MediaQuery.of(context).size.width;
    DateTime selectedDate = DateTime.utc(
        DateTime.now().year,
        DateTime.now().month,
        DateTime.now().day
    );

    return SingleChildScrollView(
      child: Container(
        child: Column(
          children: [
            // 로고
            Container(
                margin: EdgeInsets.fromLTRB(20, 20, 0, 20),
                child: Column(
                  children: [
                    // 로고
                    Container(
                      alignment: FractionalOffset.centerLeft,
                      child: Image.asset(
                        '${mainLogo}',
                        width: mediaQueryWidth * 0.15,
                        height: mediaQueryHeight * 0.15,
                      ),
                    ),
                    // 회원가입
                    Container(
                      alignment: FractionalOffset.centerLeft,
                      margin: EdgeInsets.only(left: 10),
                      child: Text(
                        "회원가입",
                        style: TextStyle(
                          fontFamily: 'NotoSans_Bold',
                          fontSize: 20,
                          letterSpacing: fontLetterSpacing,
                          color: colorPrimary,
                        ),
                      ),
                    ),
                  ],
                )
            ),
            // 회원가입 기능
            Container(
              child: Column(
                children: [
                  // 아이디 입력 창.
                  Container(
                      margin: edgeInsetsJoinBox,
                      width: mediaQueryWidth,
                      decoration: BoxDecoration(
                        color: colorLightGrey,
                        borderRadius: BorderRadius.all(
                          Radius.circular(boxRadius),
                        ),
                      ),
                      child: Column(
                        children: [
                          Container(
                            alignment: FractionalOffset.centerLeft,
                            margin: edgeInsetsJoinText,
                            child: Text(
                              '아이디',
                              style: TextStyle(
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeBig,
                                letterSpacing: fontLetterSpacing,
                                color: colorPrimary,
                              ),
                            ),
                          ),
                          Container(
                            margin: edgeInsetsJoinTextField,
                            child: TextField(
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: '사용할 아이디를 입력해 주세요.',
                                  hintStyle: TextStyle(
                                      letterSpacing: fontLetterSpacing,
                                      color: colorGrey
                                  )
                              ),
                              keyboardType: TextInputType.text,
                            ),
                          )
                        ],
                      )
                  ),

                  // 사용할 비밀번호를 입력해 주세요.
                  Container(
                      margin: edgeInsetsJoinBox,
                      width: mediaQueryWidth,
                      decoration: BoxDecoration(
                        color: colorLightGrey,
                        borderRadius: BorderRadius.all(
                          Radius.circular(boxRadius),
                        ),
                      ),
                      child: Column(
                        children: [
                          Container(
                            alignment: FractionalOffset.centerLeft,
                            margin: edgeInsetsJoinText,
                            child: Text(
                              '비밀번호',
                              style: TextStyle(
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeBig,
                                letterSpacing: fontLetterSpacing,
                                color: colorPrimary,
                              ),
                            ),
                          ),
                          Container(
                            margin: edgeInsetsJoinTextField,
                            child: TextField(
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: '사용할 비밀번호를 입력해 주세요.',
                                  hintStyle: TextStyle(
                                      letterSpacing: fontLetterSpacing,
                                      color: colorGrey
                                  )
                              ),
                              keyboardType: TextInputType.text,
                              obscureText: true,
                            ),
                          )
                        ],
                      )
                  ),

                  // 비밀번호 재확인
                  Container(
                      margin: edgeInsetsJoinBox,
                      width: mediaQueryWidth,
                      decoration: BoxDecoration(
                        color: colorLightGrey,
                        borderRadius: BorderRadius.all(
                          Radius.circular(boxRadius),
                        ),
                      ),
                      child: Column(
                        children: [
                          Container(
                            alignment: FractionalOffset.centerLeft,
                            margin: edgeInsetsJoinText,
                            child: Text(
                              '비밀번호 재확인',
                              style: TextStyle(
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeBig,
                                letterSpacing: fontLetterSpacing,
                                color: colorPrimary,
                              ),
                            ),
                          ),
                          Container(
                            margin: edgeInsetsJoinTextField,
                            child: TextField(
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: '사용할 비밀번호를 한 번 더 입력해 주세요.',
                                  hintStyle: TextStyle(
                                      letterSpacing: fontLetterSpacing,
                                      color: colorGrey
                                  )
                              ),
                              keyboardType: TextInputType.text,
                              obscureText: true,
                            ),
                          )
                        ],
                      )
                  ),

                  // 사용할 이름을 입력해 주세요.
                  Container(
                      margin: edgeInsetsJoinBox,
                      width: mediaQueryWidth,
                      decoration: BoxDecoration(
                        color: colorLightGrey,
                        borderRadius: BorderRadius.all(
                          Radius.circular(boxRadius),
                        ),
                      ),
                      child: Column(
                        children: [
                          Container(
                            alignment: FractionalOffset.centerLeft,
                            margin: edgeInsetsJoinText,
                            child: Text(
                              '이름',
                              style: TextStyle(
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeBig,
                                letterSpacing: fontLetterSpacing,
                                color: colorPrimary,
                              ),
                            ),
                          ),
                          Container(
                            margin: edgeInsetsJoinTextField,
                            child: TextField(
                              decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: '사용할 이름을 입력해주세요.',
                                  hintStyle: TextStyle(
                                      letterSpacing: fontLetterSpacing,
                                      color: colorGrey
                                  )
                              ),
                              keyboardType: TextInputType.text,
                            ),
                          )
                        ],
                      )
                  ),

                  // 생년월일
                  Container(
                      margin: edgeInsetsJoinBox,
                      width: mediaQueryWidth,
                      decoration: BoxDecoration(
                        color: colorLightGrey,
                        borderRadius: BorderRadius.all(
                          Radius.circular(boxRadius),
                        ),
                      ),
                      child: Column(
                        children: [
                          Container(
                            alignment: FractionalOffset.centerLeft,
                            margin: edgeInsetsJoinText,
                            child: Text(
                              '생년월일',
                              style: TextStyle(
                                fontFamily: 'NotoSans_Bold',
                                fontSize: fontSizeBig,
                                letterSpacing: fontLetterSpacing,
                                color: colorPrimary,
                              ),
                            ),
                          ),
                          Container(
                            alignment: FractionalOffset.centerLeft,
                            margin: EdgeInsets.fromLTRB(10, 0, 0, 10),
                            child: TextButton(
                              onPressed: () async{
                                final selectedDate = await showDatePicker(
                                  context: context, // 팝업으로 띄우기 때문에 context 전달
                                  initialDate: date, // 달력을 띄웠을 때 선택된 날짜. 위에서 date 변수에 오늘 날짜를 넣었으므로 오늘 날짜가 선택돼서 나옴
                                  firstDate: DateTime(1980), // 시작 년도
                                  lastDate: DateTime.now(), // 마지막 년도. 오늘로 지정하면 미래의 날짜 선택불가
                                );
                                if (selectedDate != null) {
                                  setState(() {
                                    date = selectedDate; // 선택한 날짜는 dart 변수에 저장
                                  });
                                }
                              }, child: Text(
                              '$date'.substring(0, 10), //출력하면 시간도 같이 출력이 되는데 날짜만 표시하고 싶어서 substring으로 잘라냄
                              style: TextStyle(
                                color: colorGrey,
                                fontSize: fontSizeMid,
                              ),
                            ),
                            ),
                          ),
                        ],
                      )
                  ),

                  // 회원 가입 하기
                  Container(
                    margin: EdgeInsets.fromLTRB(20, 50, 20, 0),
                    width: mediaQueryWidth,
                    child: ElevatedButton(
                        style: OutlinedButton.styleFrom(
                          backgroundColor: colorPrimary,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(buttonRadius),
                          ),
                        ),
                        onPressed: () async {
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => AddNewCardScreen()));
                        },
                        child: Text(
                          '회원 가입 하기',
                          style: TextStyle(
                            fontFamily: 'NotoSans_Bold',
                              fontSize: fontSizeMid,
                              letterSpacing: fontLetterSpacing,
                            color: colorInsideFont
                          ),
                        )
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      )
    );
  }
}
