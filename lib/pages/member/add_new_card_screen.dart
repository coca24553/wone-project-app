import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pattern_formatter/pattern_formatter.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_path.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/config/config_style.dart';
import 'package:wone_app/pages/member/join_membership_page.dart';
import 'package:wone_app/pages/page_index.dart';

class AddNewCardScreen extends StatefulWidget {
  const AddNewCardScreen({Key? key}) : super(key: key);

  @override
  State<AddNewCardScreen> createState() => _AddNewCardScreenState();
}

class _AddNewCardScreenState extends State<AddNewCardScreen> {
  TextEditingController cardNumberController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildBody(context),
    );
  }
  Widget _buildBody(BuildContext context) {
    double mediaQueryWidth = MediaQuery.of(context).size.width;
    double mediaQueryHeight = MediaQuery.of(context).size.width;

    return SingleChildScrollView(
      child: Column(
        children: [
          // 로고
          Container(
              margin: EdgeInsets.fromLTRB(20, 35, 0, 20),
              child: Column(
                children: [
                  // 로고
                  Container(
                    alignment: FractionalOffset.centerLeft,
                    child: Image.asset(
                      '${mainLogo}',
                      width: mediaQueryWidth * 0.2,
                      height: mediaQueryHeight * 0.2,
                    ),
                  ),
                  // 카드 등록하기
                  Container(
                    margin: EdgeInsets.fromLTRB(10, 5, 0, 0),
                    alignment: FractionalOffset.centerLeft,
                    child: Text(
                      "카드 등록하기",
                      style: TextStyle(
                        fontFamily: 'NotoSans_Bold',
                        fontSize: fontSize22,
                        letterSpacing: fontLetterSpacing,
                        color: colorPrimary,
                      ),
                    ),
                  ),
                ],
              )
          ),
          Container(
            child: Column(
              children: [
                // 카드사 선택
                Container(
                  width: mediaQueryWidth,
                  padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                  child: DropdownButtonFormField<String?>(
                    onChanged: (String? newValue) {
                      print(newValue);
                    },
                    items:
                    [null, 'NH', 'KB', 'SH', 'IBK','WOORI','KAKAO','TOSS','HN','ETC'].map<DropdownMenuItem<String?>>((String? i) {
                      return DropdownMenuItem<String?>(
                        value: i,
                        child: Text({'NH': '농협', 'KB': '국민', 'SH': '신한', 'IBK': '기업', 'WOORI': '우리', 'KAKAO': '카카오', 'TOSS': '토스', 'HN': '하나', 'ETC': '기타'}[i] ?? '카드사 선택'),
                      );
                    }).toList(),
                  ),
                ),
                // 카드번호
                Container(
                  width: mediaQueryWidth,
                  padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                  child: TextFormField(
                    controller: cardNumberController,
                    keyboardType: TextInputType.number, // 키보드 기본
                    inputFormatters: [
                      FilteringTextInputFormatter.digitsOnly, // 숫자만 입력 받고 싶을 때
                      LengthLimitingTextInputFormatter(19), // 글자수 제한
                    ],
                    decoration:
                    const InputDecoration(
                      hintText: "카드번호",
                      hintStyle: TextStyle(
                        fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                        letterSpacing: fontLetterSpacing,
                        color: colorGrey,
                      ),
                    ),
                  ),
                ),
                // MM/MY, CVV
                Container(
                  child: Row(
                    children: [
                      Container(
                        width: mediaQueryWidth / 2,
                        padding: EdgeInsets.fromLTRB(30, 0, 5, 0),
                        child: TextFormField(
                          keyboardType: TextInputType.number, // 키보드 기본
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly, // 숫자만 입력 받고 싶을 때
                            LengthLimitingTextInputFormatter(5), // 글자수 제한
                          ],
                          decoration:
                          const InputDecoration(
                            hintText: "MM/YY",
                            hintStyle: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                              letterSpacing: fontLetterSpacing,
                              color: colorGrey,
                            ),
                          ),
                        ),
                      ),
                      Container(
                        width: mediaQueryWidth / 2,
                        padding: EdgeInsets.fromLTRB(5, 0, 30, 0),
                        child: TextFormField(
                          keyboardType: TextInputType.number, // 키보드 기본
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly, // 숫자만 입력 받고 싶을 때
                            // Limit the input
                            LengthLimitingTextInputFormatter(3), // 글자수 제한
                          ],
                          decoration:
                          const InputDecoration(
                            hintText: "CSV",
                            hintStyle: TextStyle(
                              fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                              letterSpacing: fontLetterSpacing,
                              color: colorGrey,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                //etc
                Container(
                  width: mediaQueryWidth,
                  padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                  child: TextFormField(
                    keyboardType: TextInputType.number, // 키보드 기본
                    inputFormatters: [
                      FilteringTextInputFormatter.singleLineFormatter, // 입력 받고 싶을 때
                      // Limit the input
                      LengthLimitingTextInputFormatter(10), // 글자수 제한
                    ],
                    decoration:
                    const InputDecoration(
                      hintText: "MEMO",
                      hintStyle: TextStyle(
                        fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                        letterSpacing: fontLetterSpacing,
                        color: colorGrey,
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),

          // 버튼
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(10, 30, 10, 0),
                  child: Row(
                    children: [
                      Container(
                        width: mediaQueryWidth / 2.115,
                        height: 40,
                        padding: EdgeInsets.fromLTRB(20, 0, 5, 0),
                        child: ElevatedButton(
                            style: OutlinedButton.styleFrom(
                              backgroundColor: colorGrey,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                              ),
                            ),
                            onPressed: () async {
                              Navigator.pop(context);
                            },
                            child: Text(
                              '취소',
                              style: TextStyle(
                                  fontFamily: 'assets/fonts/NotoSansKR-Regular',
                                  fontSize: fontSizeMid,
                                  letterSpacing: fontLetterSpacing,
                                  color: colorInsideFont
                              ),
                            )
                        ),
                      ),
                      Container(
                        width: mediaQueryWidth / 2.115,
                        height: 40,
                        padding: EdgeInsets.fromLTRB(5, 0, 20, 0),
                        child: ElevatedButton(
                            style: OutlinedButton.styleFrom(
                              backgroundColor: colorPrimary,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(buttonRadius),
                              ),
                            ),
                            onPressed: () async {
                              Navigator.of(context).push(MaterialPageRoute(builder: (context) => PageIndex()));
                            },
                            child: Text(
                              '확인',
                              style: TextStyle(
                                  fontFamily: 'assets/fonts/NotoSansKR-Regular',
                                  fontSize: fontSizeMid,
                                  letterSpacing: fontLetterSpacing,
                                  color: colorInsideFont
                              ),
                            )
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}