import 'dart:math';

import 'package:cool_dropdown/models/cool_dropdown_item.dart';
import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_form_validator.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/config/config_style.dart';
import 'package:cool_dropdown/cool_dropdown.dart';

class SpendingPage extends StatefulWidget {
  const SpendingPage({super.key});

  @override
  State<SpendingPage> createState() => _SpendingPage();
}
List<CoolDropdownItem<String>> dropdownItemList = [];

List<String> _category = [
  '입금',
  '교육',
  '식사',
  '커피',
  '대중교통',
  '쇼핑',
  '공과금',
  '의료비',
  '문화',
  '모임',
  '구독료',
];

List<String> _images = [
  'assets/images/category/category1.png',
  'assets/images/category/category2.png',
  'assets/images/category/category3.png',
  'assets/images/category/category4.png',
  'assets/images/category/category5.png',
  'assets/images/category/category6.png',
  'assets/images/category/category7.png',
  'assets/images/category/category8.png',
  'assets/images/category/category9.png',
  'assets/images/category/category10.png',
  'assets/images/category/category11.png'
];

class _SpendingPage extends State<SpendingPage> {
  List<String> _items = []; // 리스트 선언
  String titleInput = "";
  String input = "";

  List<CoolDropdownItem<String>> dropdownItems = [];
  final dropdownController = DropdownController();

  @override
  void initState() {
    for (int i = 0; i < _category.length; i++) {
      dropdownItems.add(
        CoolDropdownItem<String>(
            label: '${_category[i]}',
            icon: Container(
              height: 25,
              width: 25,
              child: Image.asset(
               '${_images[i]}',
              ),
            ),
            value: '${_category[i]}'
        ),
      );
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    final _formKey = GlobalKey<FormState>();

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        foregroundColor: colorPrimary,
        shadowColor: Colors.black.withOpacity(appbarShadow),
        elevation: 2,
        title: Text(
          '지출 관리',
          style: TextStyle(
              fontFamily: 'NotoSans_Bold',
              fontSize: appbarFontSize,
              letterSpacing: fontLetterSpacing,
              color: colorPrimary
          ),
        ),
        actions: [
          Icon(
            Icons.notifications,
            color: colorPrimary,
            size: appbarIconSize,
          )
        ],
      ),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text(
                      "지출 추가",
                    style: TextStyle(
                        fontFamily: 'NotoSans_Bold',
                        fontSize: fontSizeBig,
                        letterSpacing: fontLetterSpacing,
                        color: colorPrimary
                    ),
                  ),
                  content: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Form(
                          key: _formKey,
                          child: Column(
                            children: [
                              CoolDropdown(
                                controller: dropdownController,
                                dropdownList: dropdownItems,
                                defaultItem: dropdownItems.first,
                                onChange: (a) {
                                  dropdownController.close();
                                },
                                resultOptions: ResultOptions(
                                  width: 70,
                                  render: ResultRender.icon,
                                  icon: SizedBox(
                                    width: 10,
                                    height: 10,
                                    child: CustomPaint(
                                      painter: DropdownArrowPainter(color: colorPrimary),
                                    ),
                                  ),
                                ),
                                dropdownOptions: DropdownOptions(
                                  width: 140,
                                ),
                                dropdownItemOptions: DropdownItemOptions(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  selectedBoxDecoration: BoxDecoration(
                                    color: colorLightGrey,
                                  ),
                                ),
                              ),
                              TextFormField(
                                keyboardType: TextInputType.text, // 키보드 기본
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return formErrorRequired;
                                  }
                                  if (value.toString().length > 15) {
                                    return formErrorMaxLength(15);
                                  }
                                },
                                onChanged: (String text) {
                                  titleInput = text;
                                },
                                decoration: InputDecoration(
                                    hintText: '지출 내용을 입력해 주세요.',
                                    hintStyle: TextStyle(
                                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                                      fontSize: fontSizeSm,
                                      letterSpacing: fontLetterSpacing,
                                      color: colorGrey,
                                    )
                                ),
                              ),
                              TextFormField(
                                keyboardType: TextInputType.number, // 키보드 기본
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return formErrorRequired;
                                  }
                                  if (value.toString().length >= 10) {
                                    return '최대 백만단위까지 가능합니다.';
                                  }
                                },
                                inputFormatters: [
                                  FilteringTextInputFormatter.digitsOnly, // 숫자만 입력 받고 싶을 때
                                  CurrencyTextInputFormatter(
                                    locale: 'ko',
                                    decimalDigits: 0,
                                    symbol: '',
                                  )
                                ],
                                onChanged: (String text) {
                                  input = text;
                                },
                                decoration: InputDecoration(
                                    hintText: '지출 금액을 입력해 주세요.',
                                    hintStyle: TextStyle(
                                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                                      fontSize: fontSizeSm,
                                      letterSpacing: fontLetterSpacing,
                                      color: colorGrey,
                                    )
                                ),
                              ),
                          ],
                      ))
                    ],
                  ),
                  actions: <Widget>[
                    TextButton(
                        onPressed: () {
                      Navigator.of(context).pop(); // 입력 후 창 닫힘
                    },
                        child: Text(
                          '취소',
                          style: TextStyle(
                            fontSize: fontSizeSm,
                            letterSpacing: fontLetterSpacing,
                          ),
                        )
                    ),
                    TextButton(
                        onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        setState(() {
                          _items.add(titleInput);
                        });
                        Navigator.of(context).pop(); // 입력 후 창 닫힘
                      }
                    },
                      child: Text(
                          '추가',
                        style: TextStyle(
                            fontSize: fontSizeSm,
                          letterSpacing: fontLetterSpacing,
                        ),
                      )
                    ),
                  ],
                );
              }
            );
          },
        label: Text('지출 추가'),
        icon: Icon(Icons.edit),
      ),
      body: _buildBody(context),
      resizeToAvoidBottomInset: false, // 키보드 올라왔을 때 화면 깨짐 현상 없애줌.
    );
  }

  Widget _buildBody(BuildContext context) {

    return ListView.builder(
      itemCount: _items.length,
        itemBuilder: (context, index) {
        return Dismissible(
            key: Key(_items[index]),
            child: Card(
              margin: EdgeInsets.all(10),
              color: colorLightGrey,
              shape: RoundedRectangleBorder(borderRadius:
              BorderRadius.circular(boxRadius),
            ),
          child: Column(
            children: [
              ListTile(
                title: Text(titleInput,
                  style: TextStyle(
                    fontSize: fontSizeMid,
                    fontFamily: 'NotoSans_Bold',
                    color: colorLightBlack,
                    letterSpacing: fontLetterSpacing,
                  ),
                ),
                trailing: Text(input,
                  style: TextStyle(
                    fontSize: fontSizeMid,
                    fontFamily: 'NotoSans_NotoSansKR-Regular',
                    color: colorGrey,
                    letterSpacing: fontLetterSpacing,
                  ),
                ),
              )
            ],
          )
        ),
        );
      }
      );
  }
}
