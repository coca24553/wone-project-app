import 'package:flutter/material.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:intl/intl.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:wone_app/config/config_style.dart';

class CalenderPage extends StatefulWidget {
  const CalenderPage({
    super.key,
  });


  @override
  State<CalenderPage> createState() => _CalenderPageState();
}

class _CalenderPageState extends State<CalenderPage> {
  DateTime selectedDate = DateTime.utc(
    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day
  );

  @override
  Widget build(BuildContext context) {
    double mediaQueryWidth = MediaQuery.of(context).size.width;
    double mediaQueryHeight = MediaQuery.of(context).size.width;

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        foregroundColor: colorPrimary,// 앱바 기본 아이콘 색
        shadowColor: Colors.black.withOpacity(0.5),
        elevation: 2,
        title: Text(
          "캘린더",
          style: TextStyle(
            fontFamily: 'NotoSans_Bold',
            fontSize: appbarFontSize,
            letterSpacing: -0.5,
            color: colorPrimary,
          ),
        ),
        actions: [
          Icon(
            Icons.notifications,
            color: colorPrimary,
            size: appbarIconSize,
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            /** 캘린더 **/
            Container(
              child: TableCalendar(
                locale: 'ko_KR', /** 한글 사용하기 **/
                onDaySelected: onDaySelected, /** 캘린더에서 날짜가 선택될때 이벤트 **/
                selectedDayPredicate: (date) {
                  return isSameDay(selectedDate, date);
                }, /** 특정 날짜가 선택된 날짜와 동일한지 여부 판단 **/
                focusedDay: DateTime.now(), /**  현재 날짜를 기준으로 달력을 표시 **/
                firstDay: DateTime(2024), /** 달력의 시작 날짜 설정 **/
                lastDay: DateTime(2034), /** 달력의 마지막 날짜 설정 **/
                headerStyle: HeaderStyle(
                  titleCentered: true, /** 타이틀을 가운데 정렬 **/
                  formatButtonVisible: false, /** 헤더에 있는 버튼 숨김 **/
                  titleTextStyle: TextStyle(
                    fontSize: fontSizeBig,
                    fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                    letterSpacing: fontLetterSpacing,
                    color: colorPrimary
                  ),
                ),
                calendarStyle: CalendarStyle(
                  todayDecoration: BoxDecoration(
                    color: colorPrimary,
                    shape: BoxShape.circle
                  )
                ),
              ),
            ),
            /** 헤더에 있는 버튼 숨김 **/
            Container(
              child: Column(
                children: [
                  Container(
                      margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      width: mediaQueryWidth,
                      height: mediaQueryWidth * 0.14,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(boxRadius),
                        ),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black12.withOpacity(0.06),
                              spreadRadius: 10,
                              blurRadius: 30,
                              offset: Offset(0,0)
                          )
                        ],
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: mediaQueryWidth * 0.5,
                            child: Row(
                              children: [
                                /** 시간 **/
                                Container(
                                  margin: EdgeInsets.fromLTRB(20, 0, 5, 0),
                                  child: Text(
                                    '08:30',
                                    style: TextStyle(
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                                      fontSize: fontSizeMicro,
                                      color: colorGrey,
                                    ),
                                  ),
                                ),
                                /** 카테고리 아이콘 **/
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Image.asset(
                                    "assets/images/category/category4.png",
                                    height: 38,
                                    width: 38,
                                  ),
                                ),
                                // 지출 내용
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Text(
                                    '커피',
                                    style: TextStyle(
                                      color: colorDark,
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_Bold',
                                      fontSize: fontSizeMid,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          /** 지출 금액 **/
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 10, 0),
                            alignment: FractionalOffset.centerRight,
                            width: mediaQueryWidth * 0.33,
                            child: Text(
                              '4,500',
                              style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                fontSize: fontSizeMid,
                                color: colorGrey,
                              ),
                            ),
                          ),
                        ],
                      )
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      width: mediaQueryWidth,
                      height: mediaQueryWidth * 0.14,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(boxRadius),
                        ),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black12.withOpacity(0.06),
                              spreadRadius: 10,
                              blurRadius: 30,
                              offset: Offset(0,0)
                          )
                        ],
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: mediaQueryWidth * 0.5,
                            child: Row(
                              children: [
                                // 시간
                                Container(
                                  margin: EdgeInsets.fromLTRB(20, 0, 5, 0),
                                  child: Text(
                                    '09:00',
                                    style: TextStyle(
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                                      fontSize: fontSizeMicro,
                                      color: colorGrey,
                                    ),
                                  ),
                                ),
                                // 카테고리 아이콘
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Image.asset(
                                    "assets/images/category/category5.png",
                                    height: 38,
                                    width: 38,
                                  ),
                                ),
                                // 지출 내용
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Text(
                                    '지하철',
                                    style: TextStyle(
                                      color: colorDark,
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_Bold',
                                      fontSize: fontSizeMid,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // 지출 금액
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 10, 0),
                            alignment: FractionalOffset.centerRight,
                            width: mediaQueryWidth * 0.33,
                            child: Text(
                              '1,250',
                              style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                fontSize: fontSizeMid,
                                color: colorGrey,
                              ),
                            ),
                          ),
                        ],
                      )
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      width: mediaQueryWidth,
                      height: mediaQueryWidth * 0.14,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(boxRadius),
                        ),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black12.withOpacity(0.06),
                              spreadRadius: 10,
                              blurRadius: 30,
                              offset: Offset(0,0)
                          )
                        ],
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: mediaQueryWidth * 0.5,
                            child: Row(
                              children: [
                                // 시간
                                Container(
                                  margin: EdgeInsets.fromLTRB(20, 0, 5, 0),
                                  child: Text(
                                    '13:00',
                                    style: TextStyle(
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                                      fontSize: fontSizeMicro,
                                      color: colorGrey,
                                    ),
                                  ),
                                ),
                                // 카테고리 아이콘
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Image.asset(
                                    "assets/images/category/category3.png",
                                    height: 38,
                                    width: 38,
                                  ),
                                ),
                                // 지출 내용
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Text(
                                    '점심',
                                    style: TextStyle(
                                      color: colorDark,
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_Bold',
                                      fontSize: fontSizeMid,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // 지출 금액
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 10, 0),
                            alignment: FractionalOffset.centerRight,
                            width: mediaQueryWidth * 0.33,
                            child: Text(
                              '11,000',
                              style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                fontSize: fontSizeMid,
                                color: colorGrey,
                              ),
                            ),
                          ),
                        ],
                      )
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      width: mediaQueryWidth,
                      height: mediaQueryWidth * 0.14,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(boxRadius),
                        ),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black12.withOpacity(0.06),
                              spreadRadius: 10,
                              blurRadius: 30,
                              offset: Offset(0,0)
                          )
                        ],
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: mediaQueryWidth * 0.5,
                            child: Row(
                              children: [
                                // 시간
                                Container(
                                  margin: EdgeInsets.fromLTRB(20, 0, 5, 0),
                                  child: Text(
                                    '13:50',
                                    style: TextStyle(
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                                      fontSize: fontSizeMicro,
                                      color: colorGrey,
                                    ),
                                  ),
                                ),
                                // 카테고리 아이콘
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Image.asset(
                                    "assets/images/category/category4.png",
                                    height: 38,
                                    width: 38,
                                  ),
                                ),
                                // 지출 내용
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Text(
                                    '커피',
                                    style: TextStyle(
                                      color: colorDark,
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_Bold',
                                      fontSize: fontSizeMid,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // 지출 금액
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 10, 0),
                            alignment: FractionalOffset.centerRight,
                            width: mediaQueryWidth * 0.33,
                            child: Text(
                              '4,500',
                              style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                fontSize: fontSizeMid,
                                color: colorGrey,
                              ),
                            ),
                          ),
                        ],
                      )
                  ),
                  Container(
                      margin: EdgeInsets.fromLTRB(10, 5, 10, 5),
                      width: mediaQueryWidth,
                      height: mediaQueryWidth * 0.14,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(
                          Radius.circular(boxRadius),
                        ),
                        boxShadow: [
                          BoxShadow(
                              color: Colors.black12.withOpacity(0.06),
                              spreadRadius: 10,
                              blurRadius: 30,
                              offset: Offset(0,0)
                          )
                        ],
                      ),
                      child: Row(
                        children: [
                          Container(
                            width: mediaQueryWidth * 0.5,
                            child: Row(
                              children: [
                                // 시간
                                Container(
                                  margin: EdgeInsets.fromLTRB(20, 0, 5, 0),
                                  child: Text(
                                    '19:00',
                                    style: TextStyle(
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_NotoSansKR-Regular',
                                      fontSize: fontSizeMicro,
                                      color: colorGrey,
                                    ),
                                  ),
                                ),
                                // 카테고리 아이콘
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Image.asset(
                                    "assets/images/category/category5.png",
                                    height: 38,
                                    width: 38,
                                  ),
                                ),
                                // 지출 내용
                                Container(
                                  margin: EdgeInsets.fromLTRB(5, 0, 5, 0),
                                  child: Text(
                                    '지하철',
                                    style: TextStyle(
                                      color: colorDark,
                                      letterSpacing: fontLetterSpacing,
                                      fontFamily: 'NotoSans_Bold',
                                      fontSize: fontSizeMid,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          // 지출 금액
                          Container(
                            margin: EdgeInsets.fromLTRB(5, 0, 10, 0),
                            alignment: FractionalOffset.centerRight,
                            width: mediaQueryWidth * 0.33,
                            child: Text(
                              '1,250',
                              style: TextStyle(
                                letterSpacing: fontLetterSpacing,
                                fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                                fontSize: fontSizeMid,
                                color: colorGrey,
                              ),
                            ),
                          ),
                        ],
                      )
                  ),
                ],
              ),
            )
          ],
        ),
      ),
      resizeToAvoidBottomInset: false, /** 키보드 올라왔을 때 화면 깨짐 현상 없애줌 **/
    );
  }

  // 달력에서 날짜가 선택됐을 때 호출되는 콜백 함수
  void onDaySelected(DateTime selectedDate, DateTime focusedDate) {
    setState(() {
      this.selectedDate = selectedDate;
    });
  }
}

