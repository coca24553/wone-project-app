import 'package:flutter/material.dart';
import 'package:wone_app/component/component_answer_item.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/model/answer/answer_item.dart';
import 'package:wone_app/pages/answer/answer_contents_page.dart';
import 'package:wone_app/repository/repo_faq_answer.dart';

class AnswerCreatePage extends StatefulWidget {
  const AnswerCreatePage({super.key});

  @override
  State<AnswerCreatePage> createState() => _AnswerCreatePageState();
}

class _AnswerCreatePageState extends State<AnswerCreatePage> {
  List<AnswerItem> _Answerlist = [];

  Future<void> _loadList() async {
    await RepoFaqAnswer().getAnswers()
        .then((res) => {
          setState(() {
            _Answerlist = res.list;
          })
    })
        .catchError((err) => {
          debugPrint(err)
    });
  }

  @override
  void initState() {
    super.initState();
    _loadList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        foregroundColor: colorPrimary,// 앱바 기본 아이콘 색
        shadowColor: Colors.black.withOpacity(0.5),
        elevation: 2,
        title: Text(
          "고객센터",
          style: TextStyle(
            fontFamily: 'NotoSans_Bold',
            fontSize: appbarFontSize,
            letterSpacing: -0.5,
            color: colorPrimary,
          ),
        ),
        actions: [
          Icon(
            Icons.search_outlined,
            color: colorPrimary,
            size: appbarIconSize,
          )
        ],
      ),
      /** component_answer_item id번 대로 가져와서 그려줘 **/
      body: ListView.builder(
        itemCount: _Answerlist.length,
        itemBuilder: (BuildContext ctx, int idx) {
          return ComponentAnswerItem(answerItem: _Answerlist[idx], callback: () {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => AnswerContentsPage(id: _Answerlist[idx].id)));
          });
        },
      ),
    );
  }
}
