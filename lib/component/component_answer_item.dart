import 'package:flutter/material.dart';
import 'package:wone_app/config/config_color.dart';
import 'package:wone_app/config/config_size.dart';
import 'package:wone_app/model/answer/answer_item.dart';

class ComponentAnswerItem extends StatelessWidget {
  const ComponentAnswerItem({
    super.key,
    required this.answerItem,
    required this.callback
  });

  final AnswerItem answerItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    double mediaQueryWidth = MediaQuery.of(context).size.width;

    return Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
            child: TextButton(
              onPressed: callback,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  /** 문의 제목 (내용 축약되서 나옴) **/
                  Container(
                    width: mediaQueryWidth * 0.5,
                    child: Text(
                      answerItem.askContent,
                      style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-SemiBold',
                          fontSize: fontSizeMid,
                          letterSpacing: fontLetterSpacing,
                          color: colorLightBlack),
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  /** 멤버 이름  **/
                  Container(
                    child: Text(
                      answerItem.memberName,
                      textAlign: TextAlign.end,
                      style: TextStyle(
                          fontFamily: 'NotoSans_NotoSansKR-Regular',
                          fontSize: fontSizeMid,
                          letterSpacing: fontLetterSpacing,
                          color: colorGrey),
                    ),
                  )
                ],
              ),
            ),
          ),
          /** 구분선 **/
          Container(
            margin: EdgeInsets.fromLTRB(0, 8, 0, 0),
            height: 1,
            color: colorLightGrey,
          ),
        ],
      ),
    );
  }
}
