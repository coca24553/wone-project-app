import 'package:flutter/material.dart';
import 'package:wone_app/model/card/card_information_item.dart';
import 'package:wone_app/model/member/member_information_item.dart';

class ComponentMemberAll extends StatelessWidget {
  const ComponentMemberAll({
    super.key,
    required this.memberInformationItem,
    required this.cardInformationItem,
    required this.callback

  });
  final MemberInformationItem memberInformationItem;
  final CardInformationItem cardInformationItem;
  final VoidCallback callback;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: callback,
      child: Column(
        children: [
          Text(
            '${memberInformationItem.id}',
          ),
          Text(
            memberInformationItem.username,
          ),
          Text(
            '${memberInformationItem.birthDate}'
          ),
          Text(
              memberInformationItem.password
          ),
          Text(
              cardInformationItem.cardGroup
          ),
          Text(
              cardInformationItem.cardNumber
          ),
          Text(
              cardInformationItem.endDate
          ),
        ],
      ),
    );
  }
}
