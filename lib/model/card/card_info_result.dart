import 'package:wone_app/model/card/card_info_response.dart';

class CardInfoResult {
  String msg;
  num code;
  CardInfoResponse data;

  CardInfoResult(this.msg, this.code, this.data);

  factory CardInfoResult.fromJson(Map<String, dynamic> json){
    return CardInfoResult(
        json['msg'],
        json['code'],
        CardInfoResponse.fromJson(json['data'])
    );
  }
}