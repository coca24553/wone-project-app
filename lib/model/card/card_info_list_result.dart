import 'package:wone_app/model/card/card_info_list_result.dart';
import 'package:wone_app/model/card/card_information_item.dart';

class CardInfoListResult{
  String msg;
  num code;
  List<CardInformationItem> list;
  num totalCount;

  CardInfoListResult(
      this.msg,
      this.code,
      this.list,
      this.totalCount
      );

  factory CardInfoListResult.fromJson(Map<String, dynamic> json){
    return CardInfoListResult(
      json['msg'],
      json['code'],
        // null이 아니면 (), null이면 [빈 배열]
      json['list'] !=null? (json['list'] as List).map((e) => CardInformationItem.fromJson(e)).toList():[],
      json['totalCount']
    );
  }
}