class CardInformationItem {
  num id;
  num memberId;
  String cardGroup;
  String cardNumber;
  String endDate;
  num cvc;
  String etcMemo;
  
  CardInformationItem(
      this.id,
      this.memberId,
      this.cardGroup,
      this.cardNumber,
      this.endDate,
      this.cvc,
      this.etcMemo
      );
  
  factory CardInformationItem.fromJson(Map<String, dynamic> json){
    return CardInformationItem(
        json['id'],
        json['memberId'],
        json['cardGroup'],
        json['cardNumber'],
        json['endDate'],
        json['cvc'],
        json['etcMemo']
    );
  }
}