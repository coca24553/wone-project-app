// 고객센터 복수용 큰틀

import 'package:wone_app/model/answer/answer_item.dart';

class AnswerItemListResult {
  String msg;
  num code;
  List<AnswerItem> list;
  num totalCount;

  AnswerItemListResult(this.msg, this.code, this.list, this.totalCount);

  factory AnswerItemListResult.fromJson(Map<String, dynamic> json) {
    return AnswerItemListResult(
        json['msg'],
        json['code'],
        json['list'] !=null ?
        (json['list'] as List).map((e) => AnswerItem.fromJson(e)).toList()
            : [],
        json['totalCount']
    );
  }
}