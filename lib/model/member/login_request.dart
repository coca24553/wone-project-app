class LoginRequest {
  String userId;
  String password;

  LoginRequest(this.userId, this.password);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['userId'] = this.userId;
    data['password'] = this.password;
    return data;
  }
}
