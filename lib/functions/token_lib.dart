import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:wone_app/pages/member/log_in_page.dart';

class TokenLib {
  static Future<String?> getMemberToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberToken');
  }
  static Future<String?> getMemberName() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString('memberName');
  }
  static void setMemberToken(String memberToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberToken', memberToken);
  }
  static void setMemberName(String memberName) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('memberName', memberName);
  }

  static void logout(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.clear();

    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => const LogInPage()),
            (route) => false
    );
  }
}
