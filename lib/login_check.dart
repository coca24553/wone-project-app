import 'package:flutter/material.dart';
import 'package:wone_app/middleware/middleware_login_check.dart';

class LoginCheck extends StatefulWidget {
  const LoginCheck({super.key});

  @override
  State<LoginCheck> createState() => _LoginCheckState();
}

class _LoginCheckState extends State<LoginCheck> {
  @override
  void initState() {
    super.initState();
    MiddlewareLoginCheck().check(context);
  }

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }
}
