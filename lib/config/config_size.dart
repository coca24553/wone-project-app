const double fontSizeSuper = 24;
const double fontSize22 = 20;
const double fontSizeBig = 18;
const double fontSizeMid = 16;
const double fontSizeSm = 14;
const double fontSizeMicro = 12;

const double fontLetterSpacing = -0.5;

const double appbarIconSize = 30; // 앱바 아이콘 사이즈
const double appbarFontSize = 20; // 앱바 폰트 사이즈

const double widthLogIn = 0.75; // 로그인 박스 가로 길이
const double heightLogIn = 0.09; // 로그인 박스 세로 길이

const double settingIconSize = 0.07; // 설정 페이지 리스트 아이콘 크기
