import 'package:dio/dio.dart';
import 'package:wone_app/config/config_api.dart';
import 'package:wone_app/model/answer/answer_contents_result.dart';
import 'package:wone_app/model/answer/answer_item_list_result.dart';
import 'package:wone_app/model/common_result.dart';

class RepoFaqAnswer {

  /**
   * 복수 R
   */
  Future<AnswerItemListResult> getAnswers() async{
    Dio dio = Dio();

    String _baseUrl = '$apiUri/faq/list/app'; // 엔드 포인트

    final response = await dio.get(
        _baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return AnswerItemListResult.fromJson(response.data);
  }

  Future<AnswerContentsResult> getAnswer(num id) async{
    Dio dio = Dio();

    String _baseUrl = '$apiUri/faq/detail/{id}'; // 엔드 포인트

    final response = await dio.get(
        _baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return AnswerContentsResult.fromJson(response.data);
  }

  Future<CommonResult> setFaqAnswer(num memberId) async {
    Dio dio = Dio();
    String _baseUrl = '$apiUri/faq/new/memberId/{memberId}'; // 엔드포인트

    final response = await dio.post(
        _baseUrl.replaceAll('{memberId}', memberId.toString()),
        options: Options(
          followRedirects: false,
          validateStatus: (status) {
            return status == 200;
          })
    );

    return CommonResult.fromJson(response.data);
  }
}