import 'package:dio/dio.dart';
import 'package:wone_app/config/config_api.dart';
import 'package:wone_app/model/member/login_request.dart';
import 'package:wone_app/model/member/login_result.dart';

class RepoMember {
  Future<LoginResult> doLogin(LoginRequest loginRequest) async {
    const String baseUrl = '$apiUri/login/member';
    Dio dio = Dio();
    final response = await dio.post(
        baseUrl,
        data: loginRequest.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );
    return LoginResult.fromJson(response.data);
  }
}