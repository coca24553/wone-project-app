import 'package:dio/dio.dart';
import 'package:wone_app/config/config_api.dart';
import 'package:wone_app/model/card/card_info_response.dart';
import 'package:wone_app/model/card/card_info_result.dart';
import 'package:wone_app/model/member/member_info_result.dart';

import '../model/card/card_info_list_result.dart';

class RepoMemberInfo {
  /**
   * 멤버 정보 단수 R
   */
  Future<MemberInfoResult> getMember(num id) async{
    Dio dio = Dio();

    String _baseUrl = '$apiUri/member/detail/admin/{id}'; // 엔드 포인트

    final response = await dio.get(
      _baseUrl.replaceAll('{id}', id.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return MemberInfoResult.fromJson(response.data);
  }


}